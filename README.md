Adam Christensen 
christe2@uoregon.edu

## ACP controle times calculator

That's "controle" with an 'e', because it's French, although "control" is also accepted. The race is called a brevet. A brevet consists of controls which are checkpoints where a bike rider must obtain proof of passage in oreder to continue in the brevet. This calculator determines the minimum and maximum control[e] times by which the rider must arrive at the location.

The times are calculated by a set of rules determined by Randonneurs USA. The formula is as follows:

The closing and opening times of a controle are determined by the time it would take a rider to reach the controle from the beginning of the brevet going the minimum speed and the maximum speed respectively. The minimum and maximum speeds are listed below. They change depending on how far away the rider is from the starting line (e.g. max opening time at 800km would be calculated: 200/34 + (400-200)/32 + (600-400)/30 + (800-600)/28)

Distance from start | Minimum Speed | Maximum Speed
             0-60km      20km/h          34km/h
           60-200km      15km/h          34km/h
          200-400km      15km/h          32km/h
          400-600km      15km/h          30km/h
         600-1000km      11.428km/h      28km/h
            +1000km      13.333km/h      26km/h

Notes: The brevet distance can be closer to the start than the final controle. The final controle is calculated as stopping at whatever the brevet distance is. 


## Authors

Initial code by Michal Young. Revised by Adam Christensen

## Replace this README

* A README.md file that includes not only identifying information (your name, email, etc.) but but also a revised, clear specification of the brevet controle time calculation rules.

